.. -*- coding: utf-8 -*-

.. Este fichero está formateado con reStructuredText para que se vea en HTML en
.. https://bitbucket.org/jgpallero/rls/. Se ha procurado que siga siendo legible
.. en formato de texto plano.

********************************************************************************
PROGRAM RLS (Robust Line Simplification)
********************************************************************************

================================================================================
GENERAL INFORMATION
================================================================================

:Title: Robust line simplification on the plane and the sphere
:Author: José Luis García Pallero
:Contact: jgpallero@gmail.com
:License: Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0)

================================================================================
COMPILATION
================================================================================

The source code of the program can be downloaded from
https://bitbucket.org/jgpallero/rls

:Note: Before the compilation, read the BUGS file.

In order to compile the program, the user must be change the directory to
``rls/`` and type the order ``make``. There are several options to pass to
``make`` (compiler selection, make the serial or parallel version, etc.), which
can be showed typing ``make help``.

After the ``make`` execution, the program is placed in the folder ``bin/``.
Executing ``bin/rls -h`` the help of the program is showed on the screen.

In order to check the executable for the plane, type

``bin/rls -t30 --form-res=' %7.2lf' example/example_plane``

after that you should obtain as result::

  % 7
    153.52  928.49
    404.41 1020.08
    546.26  903.57
    655.31  941.11
    679.28 1004.20
    630.91 1052.36
    581.17 1029.23
  NaN NaN
  % 11
     70.57  609.01
    133.07  659.34
    104.15  726.70
     37.86  714.81
     23.19  669.21
     84.03  628.63
    118.75  681.96
     60.20  716.24
     46.31  658.05
     98.78  669.93
     72.58  676.59

In order to check the executable for the sphere, type

``bin/rls -t10 -g --form-res=' %11.6lf' example/example_sphere``

after that you should obtain as result::

  % 9
    -74.000833  -53.120861
    -74.000000  -53.120444
    -73.608389  -53.306278
    -73.194972  -53.310444
    -73.485861  -53.232500
    -73.622556  -53.171667
    -74.712556  -52.771667
    -74.242556  -53.049194
    -74.000833  -53.120861

This proccess must be executed in an UNIX or compatible environment. If you use
MS Windows, change to GNU/Linux! You can also download from
https://bitbucket.org/jgpallero/rls/downloads some precompiled and statically
linked executables of the program for various operating systems.

================================================================================
DIRECTORY TREE
================================================================================

The ``rls`` package consists of several files, distributed in various
directories. There is also some scripts and text file in the main directory. The
structure is:

- ``bin/``: Stores the binary generated after the execution of the `Makefile`.
  This folder is created during the compilation proccess.
- ``example/``: Stores the files containing the example data.
- ``include/``: Stores the header C files `*.h`.
- ``lib/``: Stores the generated library. This folder is created during the
  compilation proccess.
- ``prog/``: Stores the source code of the main program.
- ``src/``: Stores the source code of the library C files `*.c`.
- ``README.rst``: This file you are reading now.
- ``BUGS``: Bug list file.
- ``Makefile``: File to compile the program.
- ``Makefile.cmp``: Auxiliary file with compiler and other details.
- ``initials``: Auxiliary script in order to create the auxiliary folders during
  the compilation.
