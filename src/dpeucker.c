/* -*- coding: utf-8 -*- */
/**
\file dpeucker.c
\brief Definición de funciones para el aligerado de polilíneas mediante un
       algoritmo basado en el de Douglas-Peucker sobre el plano y la esfera.
\author José Luis García Pallero, jgpallero@gmail.com
\note Este fichero contiene funciones paralelizadas con OpenMP.
\date 21 de marzo de 2014
\section Licencia Licencia
Copyright 2014, José Luis García Pallero, jgpallero@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/******************************************************************************/
/******************************************************************************/
#include"dpeucker.h"
/******************************************************************************/
/******************************************************************************/
int* AligeraPolilinea(const double* x,
                      const double* y,
                      const int nPtos,
                      const int incX,
                      const int incY,
                      const double tol,
                      const int paralelizaTol,
                      const enum RLS_DPEUCKER_ROBUSTO robusto,
                      const int nSegRobOrig,
                      const int nSegRobAuto,
                      const int esf,
                      int* nPtosSal)
{
    //identificador de caso especial
    int hayCasoEspecial=0;
    //valor absoluto de la tolerancia
    double atol=fabs(tol);
    //vector de salida
    int* sal=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos casos especiales
    sal = CasosEspecialesAligeraPolilinea(x,y,nPtos,incX,incY,atol,nPtosSal,
                                          &hayCasoEspecial);
    //comprobamos si ha habido algún caso especial
    if(hayCasoEspecial)
    {
        //comprobamos si ha ocurrido algún error de asignación de memoria
        if(nPtos&&(sal==NULL))
        {
            //mensaje de error
            RLS_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        else
        {
            //salimos de la función
            return sal;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si trabajamos con el algoritmo original o con el robusto
    if(robusto==RlsDPeuckerOriginal)
    {
        //versión original del algoritmo
        sal = DouglasPeuckerOriginal(x,y,nPtos,incX,incY,atol,esf,nPtosSal);
    }
    else
    {
        //utilizamos la variación robusta del algoritmo
        sal = DouglasPeuckerRobusto(x,y,nPtos,incX,incY,atol,paralelizaTol,
                                    robusto,nSegRobOrig,nSegRobAuto,esf,
                                    nPtosSal);
    }
    //comprobamos los posibles errores
    if(nPtos&&(sal==NULL))
    {
        //mensaje de error
        RLS_ERROR("Error de asignación de memoria");
        //salimos de la función
        return NULL;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return sal;
}
/******************************************************************************/
/******************************************************************************/
int* DouglasPeuckerOriginal(const double* x,
                            const double* y,
                            const int nPtos,
                            const int incX,
                            const int incY,
                            const double tol,
                            const int esf,
                            int* nPtosSal)
{
    //índice para recorrer bucles
    int i=0;
    //valor absoluto de la tolerancia
    double atol=fabs(tol);
    //variable auxiliar
    int pos=0;
    //vector de identificadores de elementos usados
    char* usados=NULL;
    //vector de salida
    int* sal=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //iniciamos la variable de salida de número de puntos a 0
    *nPtosSal = 0;
    //asigno memoria para los puntos usados y la inicializo a ceros (calloc)
    usados = (char*)calloc((size_t)nPtos,sizeof(char));
    //comprobamos los posibles errores
    if(usados==NULL)
    {
        //mensaje de error
        RLS_ERROR("Error de asignación de memoria");
        //salimos de la función
        return NULL;
    }
    //indico el primer punto y el último como usados
    usados[0] = 1;
    usados[nPtos-1] = 1;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si trabajamos sobre la esfera o sobre el plano
    if(esf)
    {
        //aplico el algoritmo original sobre la esfera
        DouglasPeuckerOriginalEsfera(x,y,nPtos,incX,incY,atol,0,nPtos-1,usados);
    }
    else
    {
        //aplico el algoritmo original sobre el plano
        DouglasPeuckerOriginalPlano(x,y,nPtos,incX,incY,atol,0,nPtos-1,usados);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //cuento los puntos usados
    for(i=0;i<nPtos;i++)
    {
        //voy contando
        (*nPtosSal) += (int)usados[i];
    }
    //asigno memoria para el vector de salida
    sal = (int*)malloc((size_t)(*nPtosSal)*sizeof(int));
    //comprobamos los posibles errores
    if(sal==NULL)
    {
        //libero la memoria utilizada
        free(usados);
        //mensaje de error
        RLS_ERROR("Error de asignación de memoria");
        //salimos de la función
        return NULL;
    }
    //inicio la variable de posición
    pos = 0;
    //recorro el vector de elementos usados
    for(i=0;i<nPtos;i++)
    {
        //compruebo si el elemento se ha usado
        if(usados[i])
        {
            //asigno la posición
            sal[pos] = i;
            //aumento el contador de posiciones en el vector de salida
            pos++;
        }
    }
    //compruebo si sólo se han usado dos puntos que son el mismo
    if((*nPtosSal==2)&&(x[0]==x[(nPtos-1)*incX])&&(y[0]==y[(nPtos-1)*incY]))
    {
        //actualizamos el número de puntos de salida
        *nPtosSal = 1;
        //reasigno memoria
        sal = (int*)realloc(sal,(size_t)(*nPtosSal)*sizeof(int));
        //comprobamos los posibles errores
        if(sal==NULL)
        {
            //libero la memoria utilizada
            free(usados);
            //mensaje de error
            RLS_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //libero la memoria utilizada
    free(usados);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return sal;
}
/******************************************************************************/
/******************************************************************************/
int* DouglasPeuckerRobusto(const double* x,
                           const double* y,
                           const int nPtos,
                           const int incX,
                           const int incY,
                           const double tol,
                           const int paralelizaTol,
                           const enum RLS_DPEUCKER_ROBUSTO robusto,
                           const int nSegRobOrig,
                           const int nSegRobAuto,
                           const int esf,
                           int* nPtosSal)
{
    //valor absoluto de la tolerancia
    double atol=fabs(tol);
    //identificador de caso especial
    int hayCasoEspecial=0;
    //vector de salida
    int* sal=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos casos especiales
    sal = CasosEspecialesAligeraPolilinea(x,y,nPtos,incX,incY,atol,nPtosSal,
                                          &hayCasoEspecial);
    //comprobamos si ha habido algún caso especial
    if(hayCasoEspecial)
    {
        //comprobamos si ha ocurrido algún error de asignación de memoria
        if(nPtos&&(sal==NULL))
        {
            //mensaje de error
            RLS_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        else
        {
            //salimos de la función
            return sal;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si trabajamos sobre la esfera o sobre el plano
    if(esf)
    {
        //aplico el algoritmo robusto sobre la esfera
        sal = DouglasPeuckerRobustoEsfera(x,y,nPtos,incX,incY,atol,
                                          paralelizaTol,robusto,nSegRobOrig,
                                          nSegRobAuto,nPtosSal);
    }
    else
    {
        //aplico el algoritmo robusto sobre el plano
        sal = DouglasPeuckerRobustoPlano(x,y,nPtos,incX,incY,tol,paralelizaTol,
                                         robusto,nSegRobOrig,nSegRobAuto,
                                         nPtosSal);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return sal;
}
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
