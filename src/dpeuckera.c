/* -*- coding: utf-8 -*- */
/**
\ingroup geom
@{
\file dpeuckera.c
\brief Declaración de funciones auxiliares para el uso de la familia de
       algoritmos de Douglas-Peucker.
\author José Luis García Pallero, jgpallero@gmail.com
\date 04 de abril de 2014
\section Licencia Licencia
Copyright 2014, José Luis García Pallero, jgpallero@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/******************************************************************************/
/******************************************************************************/
#include"dpeuckera.h"
/******************************************************************************/
/******************************************************************************/
int* CasosEspecialesAligeraPolilinea(const double* x,
                                     const double* y,
                                     const int nPtos,
                                     const int incX,
                                     const int incY,
                                     const double tol,
                                     int* nPtosSal,
                                     int* hayCasoEspecial)
{
    //índice para recorrer bucles
    int i=0;
    //valor absoluto de la tolerancia
    double atol=fabs(tol);
    //vector de salida
    int* salida=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //en principio, el número de puntos de salida es el mismo que el de entrada
    *nPtosSal = nPtos;
    //inicializamos la variable indicadora de caso especial a 0
    *hayCasoEspecial = 0;
    //vamos comprobando casos especiales
    if(nPtos==0)
    {
        //indicamos que estamos anteun caso especial
        *hayCasoEspecial = 1;
        //actualizamos la variable de número de puntos de salida
        *nPtosSal = 0;
        //salimos de la función
        return NULL;
    }
    else if(nPtos==1)
    {
        //indicamos que estamos anteun caso especial
        *hayCasoEspecial = 1;
        //asignamos memoria para el vector de salida
        salida = (int*)malloc(sizeof(int));
        //comprobamos si ha ocurrido algún error
        if(salida==NULL)
        {
            //mensaje de error
            RLS_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        //la entrada sólo es un punto
        *nPtosSal = 1;
        salida[0] = 0;
    }
    else if(nPtos==2)
    {
        //indicamos que estamos ante un caso especial
        *hayCasoEspecial = 1;
        //en principio, los dos puntos son válidos
        *nPtosSal = 2;
        //comprobamos si los puntos son o no el mismo
        if((x[0]==x[incX])&&(y[0]==y[incY]))
        {
            //sólo vale el primer punto
            *nPtosSal = 1;
        }
        //asignamos memoria para el vector de salida
        salida = (int*)malloc((size_t)(*nPtosSal)*sizeof(int));
        //comprobamos si ha ocurrido algún error
        if(salida==NULL)
        {
            //mensaje de error
            RLS_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        //copiamos los puntos válidos
        for(i=0;i<(*nPtosSal);i++)
        {
            salida[i] = i;
        }
    }
    else if((nPtos==3)&&(x[0]==x[2*incX])&&(y[0]==y[2*incY]))
    {
        //indicamos que estamos ante un caso especial
        *hayCasoEspecial = 1;
        //en principio, hay dos puntos válidos
        *nPtosSal = 2;
        //comprobamos también si el segundo punto es el mismo que el primero
        if((x[0]==x[incX])&&(y[0]==y[incY]))
        {
            //sólo vale el primer punto
            *nPtosSal = 1;
        }
        //asignamos memoria para el vector de salida
        salida = (int*)malloc((size_t)(*nPtosSal)*sizeof(int));
        //comprobamos si ha ocurrido algún error
        if(salida==NULL)
        {
            //mensaje de error
            RLS_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        //copiamos los puntos válidos
        for(i=0;i<(*nPtosSal);i++)
        {
            salida[i] = i;
        }
    }
    else if(atol==0.0)
    {
        //indicamos que estamos anteun caso especial
        *hayCasoEspecial = 1;
        //asignamos memoria para el vector de salida
        salida = (int*)malloc((size_t)nPtos*sizeof(int));
        //comprobamos si ha ocurrido algún error
        if(salida==NULL)
        {
            //mensaje de error
            RLS_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        //inicializamos el número de puntos de salida y la posición del primero
        *nPtosSal = 1;
        salida[0] = 0;
        //recorremos el resto de puntos
        for(i=1;i<nPtos;i++)
        {
            //comprobamos si el punto de trabajo es distinto al anterior
            if((x[i*incX]!=x[(i-1)*incX])&&(y[i*incY]!=y[(i-1)*incY]))
            {
                //asignamos la posición del punto de trabajo
                salida[*nPtosSal] = i;
                //actualizamos el número de puntos de salida
                (*nPtosSal)++;
            }
        }
        //comprobamos si se ha quitado algún punto
        if(nPtos!=(*nPtosSal))
        {
            //reasignamos memoria para el vector de salida
            salida = (int*)realloc(salida,(size_t)(*nPtosSal)*sizeof(int));
            //comprobamos si ha ocurrido algún error
            if(salida==NULL)
            {
                //mensaje de error
                RLS_ERROR("Error de asignación de memoria");
                //salimos de la función
                return NULL;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return salida;
}
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
