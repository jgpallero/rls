/* -*- coding: utf-8 -*- */
/**
\file errores.c
\brief Definición de funciones para el tratamiento de errores.

En el momento de la compilación ha de seleccionarse el comportamiento de la
función \ref RlsError. Para realizar la selección es necesario definir las
variables para el preprocesador \em ESCRIBE_MENSAJE_ERROR si se quiere que la
función imprima un mensaje de error y/o \em FIN_PROGRAMA_ERROR si se quiere que
la función termine la ejecución del programa en curso. Si no se define ninguna
variable, la función no ejecuta ninguna acción. En \p gcc, las variables para el
preprocesador se pasan como \em -DXXX, donde \em XXX es la variable a
introducir.
\author José Luis García Pallero, jgpallero@gmail.com
\date 06 de marzo de 2009
\section Licencia Licencia
Copyright 2013-2014, José Luis García Pallero, jgpallero@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/******************************************************************************/
/******************************************************************************/
#include"errores.h"
/******************************************************************************/
/******************************************************************************/
int RlsTipoError(void)
{
    //variable de salida
    int valor=RLS_TIPO_ERR_NADA;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distinguimos los posibles casos según las variables del preprocesador
#if defined(ESCRIBE_MENSAJE_ERROR) && defined(FIN_PROGRAMA_ERROR)
    //mensaje de error y terminación del programa
    valor = RLS_TIPO_ERR_MENS_Y_EXIT;
#elif defined(ESCRIBE_MENSAJE_ERROR)
    //mensaje de error
    valor = RLS_TIPO_ERR_MENS;
#elif defined(FIN_PROGRAMA_ERROR)
    //terminación del programa
    valor = RLS_TIPO_ERR_EXIT;
#endif
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return valor;
}
/******************************************************************************/
/******************************************************************************/
void RlsError(const char mensaje[],
              const char funcion[])
{
    //hacemos una copia para que en la compilación no dé warning si sólo se
    //termina la ejecución del programa o no se hace nada
    mensaje = mensaje;
    funcion = funcion;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distinguimos los posibles casos según las variables del preprocesador
#if defined(ESCRIBE_MENSAJE_ERROR) && defined(FIN_PROGRAMA_ERROR)
    //imprimimos el nombre de la función y el mensaje
    fprintf(stderr,"En la función '%s'\n",funcion);
    fprintf(stderr,"%s\n",mensaje);
    //indicamos que el programa finalizará
    fprintf(stderr,"El programa finalizará mediante la llamada a la función "
                   "'exit(EXIT_FAILURE)'\n");
    //detenemos la ejecución del programa
    exit(EXIT_FAILURE);
#elif defined(ESCRIBE_MENSAJE_ERROR)
    //imprimimos el nombre de la función y el mensaje
    fprintf(stderr,"En la función '%s'\n",funcion);
    fprintf(stderr,"%s\n",mensaje);
    //salimos de la función
    return;
#elif defined(FIN_PROGRAMA_ERROR)
    //indicamos que el programa finalizará
    fprintf(stderr,"El programa finalizará mediante la llamada a la función "
                   "'exit(EXIT_FAILURE)'\n");
    //detenemos la ejecución del programa
    exit(EXIT_FAILURE);
#else
    //salimos de la función
    return;
#endif
}
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
