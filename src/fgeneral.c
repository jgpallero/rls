/* -*- coding: utf-8 -*- */
/**
\file fgeneral.c
\brief Definición de funciones de utilidad general.
\author José Luis García Pallero, jgpallero@gmail.com
\date 29 de marzo de 2014
\section Licencia Licencia
Copyright 2014, José Luis García Pallero, jgpallero@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/******************************************************************************/
/******************************************************************************/
#include"fgeneral.h"
/******************************************************************************/
/******************************************************************************/
double RlsNan(void)
{
    //devolvemos el valor de NaN
    //le calculamos el valor absoluto porque, en algunos sistemas, al imprimir
    //un valor RLS_NAN normal, éste sale con un signo negativo delante
    //aclaración a 22 de septiembre de 2011: parecía que con lo del fabs()
    //estaba solucionado el asunto del signo negativo, pero no es así
    //la impresión con signo negativo parece que depende de los flags de
    //optimización al compilar y de los propios compiladores
    //no obstante, se mantiene el fabs()
    return fabs(RLS_NAN);
}
/******************************************************************************/
/******************************************************************************/
int EsRlsNan(const double valor)
{
    //comprobamos y salimos de la función
    return isnan(valor);
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
