/* -*- coding: utf-8 -*- */
/**
\file rls.c
\brief Programa para aligerar polígonos o polilíneas mediante un algoritmo
       robusto basado en el de Douglas-Peucker.
\author José Luis García Pallero, jgpallero@gmail.com
\date 25 de julio de 2013
\section Licencia Licencia
Copyright 2013-2017, José Luis García Pallero, jgpallero@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/******************************************************************************/
/******************************************************************************/
#if defined(CTOMP)
#include<omp.h>
#else
#include<time.h>
#endif
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include"dpeucker.h"
#include"getopt.h"
/******************************************************************************/
/******************************************************************************/
//versión del programa
#define PROG_VERSION "1.0.0"
//longitud máxima para los nombres de fichero
#define PROG_LON_MAX_NOM_FICH 500
//longitud máxima para leer las líneas de los ficheros de entrada
#define PROG_LON_MAX_LIN 250
//longitud máxima auxiliar
#define PROG_LON_MAX_AUX 100
//paso de grados sexagesimales a radianes
#define PROG_DR (RLS_CONST_PI/180.0)
//identificadores de los distintos tipos de algoritmos de Douglas-Peucker
#define PROG_DP_ORIG         0
#define PROG_DP_NOROB        1
#define PROG_DP_ROB_COMPLETO 2
#define PROG_DP_ROB_ALANTE   3
#define PROG_DP_ROB_ATRAS    4
//identificador NaN
#define PROG_NAN (NAN)
//valores por defecto de algunos parámetros
#define PROG_COLX    1
#define PROG_COLY    2
#define PROG_FACCOOR 1.0
#define PROG_FACTOL  1.0
#define PROG_FACRAD  "RAD"
#define PROG_ROB     PROG_DP_ROB_COMPLETO
#define PROG_GEO     0
#define PROG_RSPH    6371.0
#define PROG_ROBSO   0
#define PROG_ROBSA   0
#define PROG_PARTOL  1
#define PROG_CARCOM  '%'
#define PROG_FRES    " %22.15E"
/******************************************************************************/
/******************************************************************************/
double* ExtraeDatos(char fichDat[],int colX,int colY,double factMult,int* nFil);
double* AligeraDatos(double* puntos,int nPuntos,double tol,int robusto,
                     int robSegOrig,int robSegAuto,int geo,double rEsfera,
                     int parTol,int* nPtosAlig);
void EscribeResultados(double* puntos,int nPuntos,double factMult,
                       char formRes[]);
int* NumeroNan(double* datos,int nDatos,int* nNan);
double ExtraeElementoCadena(char cadena[],int posElemento,int* hayError);
double MarcaTiempo(void);
////////////////////////////////////////////////////////////////////////////////
void LlamadaPrograma(char nombre[]);
void MensajeAyuda(char nombre[]);
void ValParEn(int argc,char* argv[],char fichDat[],int* colX,int* colY,
              double* facCoor,double* facTol,double* tolerance,int* robust,
              int* robSegOrig,int* robSegAuto,int* geo,double* rSph,int* parTol,
              char formRes[]);
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //parámetros de entrada
    char fichDat[PROG_LON_MAX_NOM_FICH+1],formRes[PROG_LON_MAX_AUX+1];
    int colX=0,colY=0;
    double facCoor=0.0,facTol=0.0,tolerance=0.0;
    int robust=0,robSegOrig=0,robSegAuto=0,geo=0,parTol=0;
    double rSph=0.0;
    //filas de la matriz de datos de trabajo y de elementos simplificados
    int filDat=0,filAlig=0;
    //datos de trabajo
    double* datos=NULL;
    //elementos simplificados
    double* alig=NULL;
    //variable de tiempo
    double t=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos los argumentos de entrada
    ValParEn(argc,argv,fichDat,&colX,&colY,&facCoor,&facTol,&tolerance,&robust,
             &robSegOrig,&robSegAuto,&geo,&rSph,&parTol,formRes);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos el cronómetro de lectura de datos
    t = MarcaTiempo();
    //extraemos las coordenadas de los polígonos o polilíneas
    datos = ExtraeDatos(fichDat,colX,colY,facCoor,&filDat);
    //comprobamos errores
    if(datos==NULL)
    {
        //mensaje de error
        fprintf(stderr,"Error reading the data file\n");
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    //mensaje de tiempo
    fprintf(stderr,"Data reading: %lf seconds\n",MarcaTiempo()-t);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos el cronómetro de aligerado
    t = MarcaTiempo();
    //simplificamos
    alig = AligeraDatos(datos,filDat,facTol*tolerance,robust,robSegOrig,
                        robSegAuto,geo,rSph,parTol,&filAlig);
    //comprobamos errores
    if((alig==NULL)&&(filAlig==-1))
    {
        //liberamos la memoria utilizada y salimos del programa
        free(datos);
        fprintf(stderr,"Error in polygon simplification\n");
        exit(EXIT_FAILURE);
    }
    //mensaje de tiempo
    fprintf(stderr,"Work done: %lf seconds\n",MarcaTiempo()-t);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos el cronómetro para la escritura de resultados
    t = MarcaTiempo();
    //escribimos resultados en la salida estándar
    EscribeResultados(alig,filAlig,1.0/facCoor,formRes);
    //mensaje de tiempo
    fprintf(stderr,"Results written: %lf seconds\n",MarcaTiempo()-t);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    free(datos);
    free(alig);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/*
extrae los datos de trabajo de un fichero
entrada: nombre del fichero de datos
         columna de la coordenada X o la longitud
         columna de la coordenada Y o la latitud
         factor de multiplicación para las coordenadas
e/s: número de filas de la matriz de salida
salida: matriz de datos (en column major order) de dos columnas: [x y]
*/
double* ExtraeDatos(char fichDat[],int colX,int colY,double factMult,int* nFil)
{
    //índice para recorrer bucles
    int i=0;
    //identificador de fichero
    FILE* idFich=NULL;
    //cadena leída de fichero
    char linea[PROG_LON_MAX_LIN+1];
    //números de línea
    int nLin=0,posFil=0;
    //coordenadas leídas
    double x=0.0,y=0.0;
    //variable auxiliar
    int hayError=0;
    //matriz de salida
    double* datos=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //abrimos el fichero de datos
    idFich = fopen(fichDat,"rb");
    //comprobamos si ha ocurrido algún error
    if(idFich==NULL)
    {
        //lanzamos el mensaje de error
        fprintf(stderr,"Error opening the file\n%s\n",fichDat);
        //salimos de la función
        return NULL;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos el contador de filas
    *nFil = 0;
    //vamos leyendo líneas del fichero
    while(fgets(linea,PROG_LON_MAX_LIN+1,idFich)!=NULL)
    {
        //comprobamos si es una línea válida
        if((strlen(linea)>2)&&(linea[0]!=PROG_CARCOM))
        {
            //contamos la línea
            (*nFil)++;
        }
    }
    //nos colocamos de nuevo al inicio del fichero
    rewind(idFich);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos memoria para la matriz de salida
    datos = (double*)malloc((size_t)((*nFil)*2)*sizeof(double));
    //comprobamos los posibles errores
    if(datos==NULL)
    {
        //lanzamos el mensaje de error
        fprintf(stderr,"Error in memory allocation\n");
        //salimos de la función
        return NULL;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos el contador de filas
    posFil = 0;
    //vamos leyendo líneas del fichero
    while(fgets(linea,PROG_LON_MAX_LIN+1,idFich)!=NULL)
    {
        //aumentamos el contador de líneas
        nLin++;
        //comprobamos si es una línea válida
        if((strlen(linea)>2)&&(linea[0]!=PROG_CARCOM))
        {
            //extraemos la coordenada X
            x = ExtraeElementoCadena(linea,colX,&hayError);
            if(hayError!=0)
            {
                //liberamos la memoria asignada
                free(datos);
                //lanzamos el mensaje de error
                fprintf(stderr,"Error reading the line %d of file\n%s\n",
                        nLin,fichDat);
                //salimos de la función
                return NULL;
            }
            //extraemos la coordenada Y
            y = ExtraeElementoCadena(linea,colY,&hayError);
            if(hayError!=0)
            {
                //liberamos la memoria asignada
                free(datos);
                //lanzamos el mensaje de error
                fprintf(stderr,"Error reading the line %d of file\n%s\n",
                        nLin,fichDat);
                //salimos de la función
                return NULL;
            }
            //aplicamos el factor de multiplicación y almacenamos
            datos[0*(*nFil)+posFil] = factMult*x;
            datos[1*(*nFil)+posFil] = factMult*y;
            //aumentamos el contador de filas
            posFil++;
        }
    }
    //cerramos el fichero de trabajo
    fclose(idFich);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si la primera fila es de NaN
    if((datos[0]!=datos[0])||(datos[*nFil]!=datos[*nFil]))
    {
        //recorremos la primera columna desde el segundo elemento
        for(i=1;i<(*nFil);i++)
        {
            //voy retrasando una posición
            datos[i-1] = datos[i];
        }
        //recorremos la segunda columna desde el segundo elemento
        for(i=(*nFil)+1;i<2*(*nFil);i++)
        {
            //voy retrasando dos posiciones los elementos, ya que al estar la
            //matriz en column major order y ya haber retrasado la primera
            //columna, hay un espacio de más
            datos[i-2] = datos[i];
        }
        //ahora hay una fila menos
        (*nFil)--;
    }
    //comprobamos si la última fila es de NaN
    if((datos[(*nFil)-1]!=datos[(*nFil)-1])||
       (datos[2*(*nFil)-1]!=datos[2*(*nFil)-1]))
    {
        //recorremos la segunda columna desde el segundo elemento
        for(i=*nFil;i<2*(*nFil);i++)
        {
            //voy retrasando una posición
            datos[i-1] = datos[i];
        }
        //ahora hay una fila menos
        (*nFil)--;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return datos;
}
/******************************************************************************/
/******************************************************************************/
/*
simplifica los elementos de trabajo
entrada: matriz (en column major order) con las coordenadas de trabajo
         número de puntos de la matriz
         tolerancia
         identificador del tipo de algoritmo
         número de segmentos a usar para el primer algoritmo robusto
         número de segmentos a usar para el segundo algoritmo robusto (-1 error)
         identificador de trabajo sobre la esfera
         radio de la esfera
         identificación de paralelización de la comprobación de tolerancia
e/s: número de puntos aligerados
salida: matriz (en column major order) con las coordenadas aligeradas (NULL
         si error)
*/
double* AligeraDatos(double* puntos,int nPuntos,double tol,int robusto,
                     int robSegOrig,int robSegAuto,int geo,double rEsfera,
                     int parTol,int* nPtosAlig)
{
    //índices para recorrer bucles
    int i=0,j=0;
    //posición de los NaN
    int* nan=NULL;
    //número de NaN, variable de posición e índices de inicio y fin
    int nNan=0,pos=0,posIni=0,posFin=0;
    //indicador para añadir NaN
    int anyadeNan=0;
    //vectores de coordenadas
    double* x=NULL;
    double* y=NULL;
    //vectores auxiliares
    double* xAux=NULL;
    double* yAux=NULL;
    //posiciones de puntos
    int* alig=NULL;
    //número de puntos de la entidad aligerada
    int nPtosSal=0;
    //tipo de algoritmo a utilizar
    enum RLS_DPEUCKER_ROBUSTO rob=RlsDPeuckerRobNo;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos el tipo de algoritmo
    switch(robusto)
    {
        case PROG_DP_ORIG:
            //algoritmo original
            rob = RlsDPeuckerOriginal;
            break;
        case PROG_DP_NOROB:
            //algoritmo no robusto
            rob = RlsDPeuckerRobNo;
            break;
        case PROG_DP_ROB_COMPLETO:
            //algoritmo robusto
            rob = RlsDPeuckerRobSi;
            break;
        case PROG_DP_ROB_ALANTE:
            //algoritmo semi robusto de intersección con el elemento original
            rob = RlsDPeuckerRobOrig;
            break;
        case PROG_DP_ROB_ATRAS:
            //algoritmo semi robusto de auto intersección
            rob = RlsDPeuckerRobAuto;
            break;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //buscamos los NaN
    nan = NumeroNan(puntos,nPuntos,&nNan);
    //comprobamos errores
    if((nan==NULL)&&(nNan==-1))
    {
        //indicamos que ha ocurrido un error
        *nPtosAlig = -1;
        fprintf(stderr,"Error in NaN count\n");
        //salimos de la función
        return NULL;
    }
    //inicializamos el contador de puntos
    *nPtosAlig = nNan;
    //inicializamos los vectores de coordenadas con el número de NaN
    if(nNan)
    {
        //asignamos memoria
        x = (double*)malloc((size_t)nNan*sizeof(double));
        y = (double*)malloc((size_t)nNan*sizeof(double));
        //comprobamos los posibles errores
        if((x==NULL)||(y==NULL))
        {
            //liberamos la memoria asignada
            free(nan);
            free(x);
            free(y);
            //indicamos que ha ocurrido un error
            *nPtosAlig = -1;
            fprintf(stderr,"Error in memory allocation\n");
            //salimos de la función
            return NULL;
        }
    }
    //recorremos el número de elementos
    for(i=0;i<(nNan+1);i++)
    {
        //comprobamos si estamos ante el primer elemento, el último u otro
        if(i==0)
        {
            //posición de inicio del elemento
            posIni = 0;
            //posición de fin del elemento
            if(nNan==0)
            {
                posFin = nPuntos;
                anyadeNan = 0;
            }
            else
            {
                posFin = nan[0];
                anyadeNan = 1;
            }
        }
        else if(i==nNan)
        {
            //posiciones de inicio y fin
            posIni = nan[i-1]+1;
            posFin = nPuntos;
            anyadeNan = 0;
        }
        else
        {
            //posiciones de inicio y fin
            posIni = nan[i-1]+1;
            posFin = nan[i];
            anyadeNan = 1;
        }
        //comprobamos si trabajamos sobre la esfera
        if(geo)
        {
            //pasamos la tolerancia a arco de círculo máximo
            tol /= rEsfera;
        }
        //aligeramos
        nPtosSal = 0;
        xAux = &puntos[0*nPuntos+posIni];
        yAux = &puntos[1*nPuntos+posIni];
        alig = AligeraPolilinea(xAux,yAux,posFin-posIni,1,1,tol,parTol,rob,
                                robSegOrig,robSegAuto,geo,&nPtosSal);
        //comprobamos los posibles errores
        if(alig==NULL)
        {
            //liberamos la memoria asignada
            free(nan);
            free(x);
            free(y);
            //indicamos que ha ocurrido un error
            *nPtosAlig = -1;
            fprintf(stderr,"Error in the simplification function\n");
            //salimos de la función
            return NULL;
        }
        //actualizamos el contador de puntos totales
        (*nPtosAlig) += nPtosSal;
        //reasignamos memoria a los vectores de coordenadas
        x = (double*)realloc(x,(size_t)(*nPtosAlig)*sizeof(double));
        y = (double*)realloc(y,(size_t)(*nPtosAlig)*sizeof(double));
        //comprobamos los posibles errores
        if((x==NULL)||(y==NULL))
        {
            //liberamos la memoria asignada
            free(nan);
            free(alig);
            free(x);
            free(y);
            //indicamos que ha ocurrido un error
            *nPtosAlig = -1;
            fprintf(stderr,"Error in memory allocation\n");
            //salimos de la función
            return NULL;
        }
        //recorremos los puntos aligerados
        for(j=0;j<nPtosSal;j++)
        {
            //asignamos las coordenadas
            x[pos] = xAux[alig[j]];
            y[pos] = yAux[alig[j]];
            //aumentamos el contador de posiciones
            pos++;
        }
        //liberamos memoria
        free(alig);
        //comprobamos si hay que añadir NaN
        if(anyadeNan)
        {
            //asignamos las coordenadas
            x[pos] = PROG_NAN;
            y[pos] = PROG_NAN;
            //aumentamos el contador de posiciones
            pos++;
        }
    }
    //asignamos memoria para crear la matriz
    x = (double*)realloc(x,(size_t)(2*(*nPtosAlig))*sizeof(double));
    //comprobamos los posibles errores
    if(x==NULL)
    {
        //liberamos la memoria asignada
        free(nan);
        free(x);
        free(y);
        //indicamos que ha ocurrido un error
        *nPtosAlig = -1;
        fprintf(stderr,"Error in memory allocation\n");
        //salimos de la función
        return NULL;
    }
    //recorremos el vector y
    for(i=0;i<(*nPtosAlig);i++)
    {
        x[pos] = y[i];
        pos++;
    }
    //liberamos la memoria asignada
    free(nan);
    free(y);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return x;
}
/******************************************************************************/
/******************************************************************************/
/*
escribe los resultados en la salida estándar
entrada: matriz (en column major order) con los elementos simplificados
         número de puntos en la matriz (incluidas las filas de NaN)
         número de entidades, polígonos o polilíneas
         factor para multiplicar las coordenadas antes de imprimirlas
         formato de escritura de coordenadas
*/
void EscribeResultados(double* puntos,int nPuntos,double factMult,
                       char formRes[])
{
    //índices para recorrer bucles
    int i=0,j=0;
    //posición de los NaN
    int* nan=NULL;
    //número de NaN e índices de inicio y fin
    int nNan=0,posIni=0,posFin=0;
    //indicador para añadir NaN
    int anyadeNan=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //buscamos los NaN
    nan = NumeroNan(puntos,nPuntos,&nNan);
    //comprobamos errores
    if((nan==NULL)&&(nNan==-1))
    {
        fprintf(stderr,"Error in NaN count\n");
        //salimos de la función
        return;
    }
    //recorremos el número de elementos
    for(i=0;i<(nNan+1);i++)
    {
        //comprobamos si estamos ante el primer elemento, el último u otro
        if(i==0)
        {
            //posición de inicio del elemento
            posIni = 0;
            //posición de fin del elemento
            if(nNan==0)
            {
                posFin = nPuntos;
                anyadeNan = 0;
            }
            else
            {
                posFin = nan[0];
                anyadeNan = 1;
            }
        }
        else if(i==nNan)
        {
            //posiciones de inicio y fin
            posIni = nan[i-1]+1;
            posFin = nPuntos;
            anyadeNan = 0;
        }
        else
        {
            //posiciones de inicio y fin
            posIni = nan[i-1]+1;
            posFin = nan[i];
            anyadeNan = 1;
        }
        //escribimos el comentario
        fprintf(stdout,"%c %d\n",PROG_CARCOM,posFin-posIni);
        //recorremos el número de puntos
        for(j=posIni;j<posFin;j++)
        {
            //coordenada x
            fprintf(stdout,formRes,puntos[0*nPuntos+j]*factMult);
            //coordenada y
            fprintf(stdout,formRes,puntos[1*nPuntos+j]*factMult);
            //salto de línea
            fprintf(stdout,"\n");
        }
        //comprobamos si hay que añadir NaN
        if(anyadeNan)
        {
            fprintf(stdout,"NaN NaN\n");
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
/*
busca las filas en las que se amacena el valor NaN
entrada: matriz (en column major order) de coordenadas de datos
         número de puntos en la matriz (incluidas las filas de NaN)
e/s: número de NaN (-1 si ha ocurrido algún error)
salida: vector con los índices donde hay NaN (NULL si ocurre algún error o no
        hay NaN)
*/
int* NumeroNan(double* datos,int nDatos,int* nNan)
{
    //índice para recorrer bucles
    int i=0;
    //variable de posición
    int pos=0;
    //vector de salida
    int* nan=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos el número de NaN a 0
    *nNan = 0;
    //contamos los NaN
    for(i=0;i<nDatos;i++)
    {
        //un nan no es igual a sí mismo en punto flotante
        //como estamos en column major order trato a la matriz como un vector
        if(datos[i]!=datos[i])
        {
            (*nNan)++;
        }
    }
    //sólo seguimos si hay algún NaN
    if((*nNan)>0)
    {
        //asigno memoria
        nan = (int*)malloc((size_t)(*nNan)*sizeof(int));
        //comprobamos errores
        if(nan==NULL)
        {
            fprintf(stderr,"Error in memory allocation\n");
            //salimos de la función
            return NULL;
        }
        //recorremos los datos
        for(i=0;i<nDatos;i++)
        {
            //comprobamos si es NaN
            if(datos[i]!=datos[i])
            {
                //asignamos el índice
                nan[pos] = i;
                //aumentamos el contador de posición
                pos++;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return nan;
}
/******************************************************************************/
/******************************************************************************/
/*
Extrae el elemento numérico de tipo double de una posición dada de una cadena
entrada: cadena de texto de trabajo
         posición a extraer
e/s: identificador de error: 0/1 -> no hay error/sí lo hay
salida: número leído
*/
double ExtraeElementoCadena(char cadena[],
                            int posElemento,
                            int* hayError)
{
    //índice para recorrer bucles
    int i=0;
    //subcadena para leer los elementos
    char subcadena[PROG_LON_MAX_AUX+1];
    //dirección de inicio de la subcadena leída en la cadena original
    char* iniSub=NULL;
    //puntero de salida para strtod()
    char* sal=NULL;
    //longitud de la cadena original
    size_t lon=strlen(cadena);
    //posición en la cadena original
    size_t pos=0;
    //variable de salida
    double valor=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la variable de error a 0.0
    *hayError = 0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos el número de elementos a leer
    for(i=0;i<=posElemento;i++)
    {
        //leemos el elemento correspondiente de la cadena original
        if(sscanf(&cadena[pos],"%s",subcadena)==1)
        {
            //buscamos la posición inicial de la cadena leída
            iniSub = strstr(&cadena[pos],subcadena);
            //actualizamos la posición para leer la siguiente subcadena
            pos = lon-strlen(iniSub)+strlen(subcadena);
        }
        else
        {
            //ha ocurrido un error en la lectura
            *hayError = 1;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si no hay error
    if(!(*hayError))
    {
        //intentamos convertir a número
        sal = subcadena;
        valor = strtod(subcadena,&sal);
        //comprobamos los posibles errores
        if((valor==0.0)&&(subcadena[0]==sal[0]))
        {
            //la cadena comienza con un carácter no numérico
            *hayError = 1;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return valor;
}
/******************************************************************************/
/******************************************************************************/
/*
función para obtener los segundos pasados desde el inicio del programa
salida: segundos pasados desde el inicio del programa
*/
double MarcaTiempo(void)
{
    //distinguimos entre cálculo con las rutinas de OpenMP y con las de time.h
#if defined(CTOMP)
    //devolvemos la marca de tiempo con la función de OpenMP
    return omp_get_wtime();
#else
    //devolvemos la marca de tiempo con la función de C
    return ((double)clock())/((double)CLOCKS_PER_SEC);
#endif
}
/******************************************************************************/
/******************************************************************************/
/*
función para mostrar en pantalla las posibles llamadas al programa
entrada: nombre del programa
*/
void LlamadaPrograma(char nombre[])
{
    fprintf(stderr,"%s [options] --tolerance|-t file\n",nombre);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/*
función para mostrar en pantalla la ayuda del programa
entrada: nombre del programa
*/
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,
"Simplifies polygons or polylines using a derived Douglas-Peucker algorithm.\n"
"The implemented version is robust in the sense it will not produces\n"
"self-intersections in the resulting entity\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Use of the program %s:\n",nombre);
    fprintf(stderr,"\n");
    LlamadaPrograma(nombre);
    fprintf(stderr,"\n");
    fprintf(stderr,"Mandatory options "
                   "(--op-long=val|-op-short val|-op-shortval):\n");
    fprintf(stderr,"\n");
    fprintf(stderr,
"\tfile: File storing the coordinates of the vertices of the working polygons\n"
"\t      or polylines. The file can contain empty lines\n"
"\t--tolerance|-t: Tolerance for vertex elimination. For simplification on\n"
"\t                the plane, the value must be introduced in the same units\n"
"\t                as the coordinates of the working vertices. If the\n"
"\t                simplification works on the sphere (--geo|-g), this value\n"
"\t                must be introduced in the same units as for the radius of\n"
"\t                the working sphere (--r-sphere)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Options with arguments "
                   "(--op-long=val|-op-short val|-op-shortval):\n");
    fprintf(stderr,"\n");
    fprintf(stderr,
"\t--col-x|-x: Column in the data file for the X coordinate or longitude of\n"
"\t            the polyline vertices\n"
"\t            By default: %d\n"
"\t--col-y|-y: Column in the data file for the Y coordinate or latitude of\n"
"\t            the polyline vertices\n"
"\t            By default: %d\n"
"\t--fac-coor|-f: Multiplicative factor to apply (only for internal\n"
"\t               computations) to the coordinates of the entities vertices.\n"
"\t               If this value is the string '%s' (uppercase or lowercase)\n"
"\t               the factor to convert sexagesimal degrees to radians is\n"
"\t               employed\n"
"\t               By default: %g\n"
"\t               By default if --geo|-g: %s\n"
"\t--fac-tol: Multiplicative factor to apply to the introduced tolerance\n"
"\t           value\n"
"\t           By default: %g\n"
"\t--robust|-r: Identifier for the algorithm to use. Four possibilities:\n"
"\t              - %d: Original Douglas-Peucker algorithm (non-robust)\n"
"\t              - %d: Non-robust and non-recursive variation of the\n"
"\t                   Douglas-Peucker algorithm\n"
"\t              - %d: Robust variation of the Douglas-Peucker algorithm\n"
"\t              - %d: Semi-robust variation of the Douglas-Peucker. It\n"
"\t                   guarantees that the segments of the simplified\n"
"\t                   polyline, when created, do not intersect with the rest\n"
"\t                   of the original line still not proccessed. In special\n"
"\t                   cases, sef-intersection can still appear using this\n"
"\t                   algorithm\n"
"\t              - %d: Semi-robust variation of the Douglas-Peucker. It\n"
"\t                   guarantees that the segments of the simplified\n"
"\t                   polyline, when created, do not intersect with the\n"
"\t                   previously created segments of the same simplified\n"
"\t                   line. In special cases, sef-intersection can still\n"
"\t                   appear using this algorithm\n"
"\t              By default: %d\n"
"\t--rob-seg-orig: Number of segmrnts of the original entities to use if the\n"
"\t                --robust|-r option equals %d or %d. If the value 0 is\n"
"\t                assigned, all segments until the end of the original\n"
"\t                entities will be used\n"
"\t                By default: %d\n"
"\t--rob-seg-auto: Number of segments of the simplified entities to use if\n"
"\t                the --robust|-r option equals %d or %d. If the value 0 is\n"
"\t                assigned, all segments until the start of the original\n"
"\t                entities will be used\n"
"\t                By default: %d\n"
"\t--r-sphere: Radius of the working sphere. It must have the same units as\n"
"\t            the --tolerance|-t argument. This argument is only taken into\n"
"\t            account if the working surface is the sphere (--geo|-g)\n"
"\t            By default: %.4lf\n"
"\t--form-res: Format string in C for coordinate printing. It cannot be empty\n"
"\t            By default: '%s'\n",
PROG_COLX,PROG_COLY,PROG_FACRAD,PROG_FACCOOR,PROG_FACRAD,PROG_FACTOL,
PROG_DP_ORIG, PROG_DP_NOROB,PROG_DP_ROB_COMPLETO,PROG_DP_ROB_ALANTE,
PROG_DP_ROB_ATRAS, PROG_ROB,PROG_DP_ROB_COMPLETO,PROG_DP_ROB_ALANTE,PROG_ROBSO,
PROG_DP_ROB_COMPLETO,PROG_DP_ROB_ATRAS,PROG_ROBSA,PROG_RSPH,PROG_FRES);
    fprintf(stderr,"\n");
    fprintf(stderr,"Otions without arguments:\n");
    fprintf(stderr,"\n");
    fprintf(stderr,
"\t--no-par-tol|-n: Although this executable is compiled in parallel, the\n"
"\t                 tests for checking if a point is in tolerance will be\n"
"\t                 performed in serial mode\n"
"\t--geo|-g: The simpification will be carried out on the surface of the\n"
"\t          sphere of radius --r-sphere. Internally, the coordinates of the\n"
"\t          polylines vertices must be in radians, so the coordinates in the\n"
"\t          working files should be converted to this units (see\n"
"\t          --fac-coor|-f option)\n"
"\t--help|-h: Shows this help in the error output (stderr)\n"
"\t--version|-v: Shows the program version\n");
    fprintf(stderr,"\n");
    fprintf(stderr,
"**The data file can contain various polygons or polylines. Each entity must\n"
"  be separated by a row of vertices of coordinates (NaN,NaN). The file can\n"
"  also contain empty and/or comment lines, which must start (without spaces\n"
"  before) with the character '%c'. The file will be structured by columns in\n"
"  the form:\n"
"  - Col. --col-x|-x: Coordinate X or longitude of the vertex\n"
"  - Col. --col-y|-y: Coordinate Y or latitude of the vertex\n"
"  If the entity is a polygon, the coordinates of the first vertex must be\n"
"  repeated in the last position\n"
"**The results will be printed in the standard output (stdout), and will have\n"
"  for each entity the format:\n"
"  - Lines with the coordinates of the vertices. The columns are:\n"
"    - Col. 1: Coordinate X or longitude of the vertex\n"
"    - Col. 2: Coordinate Y or latitude of the vertex\n"
"  For each entity, a comment line containing the number of vertices it is\n"
"  composed will be printed\n"
"  As a separator between each entity, a line containing NaN NaN will be\n"
"  printed\n",PROG_CARCOM);
    fprintf(stderr,"\n");
    fprintf(stderr,
"**Results are printed in the standard output (stdout)\n"
"**Info, warning and error messages are printed in error output (stdout)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,
"Copyright (C) José Luis García Pallero, 2013-2014, jgpallero@gmail.com\n"
"This program is under the terms of the Apache 2.0 license\n"
"See http://www.apache.org/licenses/LICENSE-2.0\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Use of the program %s:\n",nombre);
    fprintf(stderr,"\n");
    LlamadaPrograma(nombre);
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/*
función para extraer los argumentos de entrada
entrada: número de argumentos de entrada
         lista de argumentos de entrada
e/s: nombre del fichero de datos
     columna de las coordenadas X
     columna de las coordenadas Y
     factor de multiplicación a aplicar a las coordenadas de entrada
     factor de multiplicación a aplicar a la tolerancia
     tolerancia para el aligerado
     identificador del algoritmo
     número de segmentos a usar para el primer algoritmo robusto
     número de segmentos a usar para el segundo algoritmo robusto
     identificador de trabajo sobre la superficie de la esfera
     radio de la esfera de trabajo
     identificador para comprobar en paralelo si los puntos están en tolerancia
     cadena de comentario
     cadena de formato para los resultados
*/
void ValParEn(int argc,char* argv[],char fichDat[],int* colX,int* colY,
              double* facCoor,double* facTol,double* tolerance,int* robust,
              int* robSegOrig,int* robSegAuto,int* geo,double* rSph,int* parTol,
              char formRes[])
{
    //identificadores de que se han pasado factores de escala
    int usaFacCoor=0,usaFacTol=0;
    //factores de escala
    char facCoorTxt[PROG_LON_MAX_AUX+1],facTolTxt[PROG_LON_MAX_AUX+1];
    //valor que devuelve getopt_long: siguiente opción
    int sigOp=0;
    //índice de la opción larga en la estructura
    int indOp=0;
    //lista de opciones con nombre corto
    char opCortas[]="x:y:f:t:r:gnhv";
    //lista de opciones con nombre largo
    struct option opLargas[] =
    {
        //nombre,         tipo,              flag, val      indOp
        {"col-x",         required_argument, NULL, 'x'}, //  0
        {"col-y",         required_argument, NULL, 'y'}, //  1
        {"fac-coor",      required_argument, NULL, 'f'}, //  2
        {"fac-tol",       required_argument, NULL,  0 }, //  3
        {"tolerance",     required_argument, NULL, 't'}, //  4
        {"robust",        required_argument, NULL, 'r'}, //  5
        {"rob-seg-orig",  required_argument, NULL,  0 }, //  6
        {"rob-seg-auto",  required_argument, NULL,  0 }, //  7
        {"geo",           no_argument,       NULL, 'g'}, //  8
        {"r-sphere",      required_argument, NULL,  0 }, //  9
        {"no-par-tol",    no_argument,       NULL, 'n'}, // 10
        {"form-res",      required_argument, NULL,  0 }, // 11
        {"help",          no_argument,       NULL, 'h'}, // 12
        {"version",       no_argument,       NULL, 'v'}, // 13
        {0,               0,                 0,     0 }
    };
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos por defecto los argumentos
    strcpy(fichDat,"");
    *colX = PROG_COLX;
    *colY = PROG_COLY;
    *facCoor = PROG_FACCOOR;
    *facTol = PROG_FACTOL;
    *tolerance = -1.0;
    *robust = PROG_ROB;
    *robSegOrig = PROG_ROBSO;
    *robSegAuto = PROG_ROBSA;
    *geo = PROG_GEO;
    *rSph = PROG_RSPH;
    *parTol = PROG_PARTOL;
    strcpy(formRes,PROG_FRES);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si se ha pasado algún argumento
    if(argc==1)
    {
        //lanzamos el mensaje de error
        fprintf(stderr,"Incorrect call to the program\nTry %s -h\n",argv[0]);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //entramos en un bucle hasta que se acaben los argumentos: getopt==-1
    while((sigOp=getopt_long(argc,argv,opCortas,opLargas,&indOp))!=-1)
    {
        //analizamos la opción extraída
        switch(sigOp)
        {
            case 'x':
                //columna de coordenadas X
                *colX = atoi(optarg);
                if((*colX)<1)
                {
                    //mensaje de error y salimos del programa
                    fprintf(stderr,"Option --col-x|-x not correct\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 'y':
                //columna de coordenadas Y
                *colY = atoi(optarg);
                if((*colY)<1)
                {
                    //mensaje de error y salimos del programa
                    fprintf(stderr,"Option --col-y|-y not correct\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 'f':
                //factor de multiplicación para las coordenadas
                usaFacCoor = 1;
                strcpy(facCoorTxt,optarg);
                if(!strcmp(facCoorTxt,""))
                {
                    //mensaje de error y salimos del programa
                    fprintf(stderr,"Option --fac-coor|-f not correct\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 't':
                //tolerancia para el aligerado
                *tolerance = atof(optarg);
                if((*tolerance)<0.0)
                {
                    //mensaje de error y salimos del programa
                    fprintf(stderr,"Option --tolerance|-t not correct\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 'r':
                //algoritmo robusto
                *robust = atoi(optarg);
                if(((*robust)!=PROG_DP_ORIG)&&
                   ((*robust)!=PROG_DP_NOROB)&&
                   ((*robust)!=PROG_DP_ROB_COMPLETO)&&
                   ((*robust)!=PROG_DP_ROB_ALANTE)&&
                   ((*robust)!=PROG_DP_ROB_ATRAS))
                {
                    //mensaje de error y salimos del programa
                    fprintf(stderr,"Option --robust|-r not correct\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 'g':
                //trabajamos con coordenadas geodésicas
                *geo = 1;
                break;
            case 'n':
                //no paralelizamos la comprobación de tolerancia
                *parTol = 0;
                break;
            case 'h':
                //imprimimos la ayuda y salimos del programa
                MensajeAyuda(argv[0]);
                exit(EXIT_FAILURE);
            case 'v':
                //imprimimos la version y salimos del programa
                fprintf(stderr,"%s %s\n"
      "Copyright (C) José Luis García Pallero, 2013-2014, jgpallero@gmail.com\n"
                   "This program is under the terms of the Apache 2.0 license\n"
                             "See http://www.apache.org/licenses/LICENSE-2.0\n",
                        argv[0],PROG_VERSION);
                exit(EXIT_FAILURE);
            case 0:
                //comprobamos el índice de la opción
                switch(indOp)
                {
                    case 3:
                        //factor de multiplicación para la tolerancia
                        usaFacTol = 1;
                        strcpy(facTolTxt,optarg);
                        if(!strcmp(facTolTxt,""))
                        {
                            //mensaje de error y salimos del programa
                            fprintf(stderr,"Option --fac-tol not correct\n");
                            exit(EXIT_FAILURE);
                        }
                        break;
                    case 6:
                        //número de vértices para el algoritmo robusto
                        *robSegOrig = atoi(optarg);
                        if((*robSegOrig)<0)
                        {
                            //mensaje de error y salimos del programa
                            fprintf(stderr,
                                    "Option --rob-seg-orig not correct\n");
                            exit(EXIT_FAILURE);
                        }
                        break;
                    case 7:
                        //número de segmentos para el algoritmo robusto
                        *robSegAuto = atoi(optarg);
                        if((*robSegAuto)<0)
                        {
                            //mensaje de error y salimos del programa
                            fprintf(stderr,
                                    "Option --rob-seg-auto not correct\n");
                            exit(EXIT_FAILURE);
                        }
                        break;
                    case 11:
                        //formato para la impresión de resultados
                        strcpy(formRes,optarg);
                        if(!strcmp(formRes,""))
                        {
                            //mensaje de error y salimos del programa
                            fprintf(stderr,"Option --form-res not correct\n");
                            exit(EXIT_FAILURE);
                        }
                        break;
                    default:
                        //imprimimos el mensaje de error y salimos del programa
                        fprintf(stderr,"Option not correct\n");
                        exit(EXIT_FAILURE);
                }
                //salimos del switch
                break;
            default:
                //imprimimos el mensaje de error y salimos del programa
                fprintf(stderr,"Option not correct\n");
                exit(EXIT_FAILURE);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos el fichero de trabajo
    if(argv[optind]!=NULL)
    {
        //extraemos el nombre del fichero
        strcpy(fichDat,argv[optind]);
        //comprobamos si es válido
        if(!strcmp(fichDat,""))
        {
            //lanzamos el mensaje de error y salimos del programa
            fprintf(stderr,"Missed data file\n");
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        //lanzamos el mensaje de error y salimos del programa
        fprintf(stderr,"Missed data file\n");
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si se ha pasado la tolerancia
    if((*tolerance)<0.0)
    {
        //lanzamos el mensaje de error y salimos del programa
        fprintf(stderr,"The argument --tolerance|-t is mandatory\n");
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si se ha pasado factor para las cordenadas
    if(usaFacCoor)
    {
        //comprobamos si se ha indicado el paso de grados a radianes
        if((!strcmp(facCoorTxt,PROG_FACRAD)))
        {
            //paso de grados sexagesimales en formato decimal a radianes
            *facCoor = PROG_DR;
        }
        else
        {
            //asignamos el número pasado
            *facCoor = atof(facCoorTxt);
            if((*facCoor)==0.0)
            {
                //mensaje de error y salimos del programa
                fprintf(stderr,"Option --fac-coor|-f not correct\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    else
    {
        //comprobamos el tipo de coordenadas de trabajo
        if(*geo)
        {
            //paso de grados sexagesimales en formato decimal a radianes
            *facCoor = PROG_DR;
        }
    }
    //comprobamos si se ha pasado factor para la tolerancia
    if(usaFacTol)
    {
        //asignamos el número pasado
        *facTol = atof(facTolTxt);
        if((*facTol)==0.0)
        {
            //mensaje de error y salimos del programa
            fprintf(stderr,"Option --fac-tol not correct\n");
            exit(EXIT_FAILURE);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si las columnas no son la misma
    if((*colX)==(*colY))
    {
        //mensaje de error y salimos del programa
        fprintf(stderr,
                "The values --col-x|-x and --col-y|-y can't be the same\n");
        exit(EXIT_FAILURE);
    }
    //ajustamos a inicio en 0 las opciones que representan índices
    (*colX)--;
    (*colY)--;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
