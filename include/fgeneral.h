/* -*- coding: utf-8 -*- */
/**
\file fgeneral.h
\brief Declaración de macros y funciones de utilidad general.
\author José Luis García Pallero, jgpallero@gmail.com
\note Este fichero contiene funciones paralelizadas con OpenMP.
\date 25 de septiembre de 2009
\version 1.0
\section Licencia Licencia
Copyright 2013-2014, José Luis García Pallero, jgpallero@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _FGENERAL_H_
#define _FGENERAL_H_
/******************************************************************************/
/******************************************************************************/
#include<math.h>
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_CONST_PI
\brief Constante \em PI.
\date 02 de marzo de 2009: Creación de la constante.
*/
#define RLS_CONST_PI (3.14159265358979323846264338328)
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_NAN
\brief Constante \em Not-a-Number (\em NaN). Se define de la misma manera que en
       la biblioteca GSL. Si la biblioteca estándar provee la constante \p NAN,
       se define directamente como \p NAN; si provee sólo la constante
       \p INFINITY, se define como \p INFINITY/INFINITY; mientras que si no
       provee ninguna de las anteriores se define directamente como \p 0.0/0.0.
       Esto lo he hecho así porque, aunque he buscado y hecho pruebas, todavía
       no sé seguro si la constante \b NAN pertenece a \p C99 o no.
\date 21 de diciembre de 2010: Creación de la constante.
\date 22 de mayo de 2012: Definición de constante a partir de las constantes
      \b NAN o \b INFINITY, si existen.
*/
#if defined(NAN)
#define RLS_NAN (NAN)
#elif defined(INFINITY)
#define RLS_NAN (INFINITY/INFINITY)
#else
#define RLS_NAN (0.0/0.0)
#endif
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_MAX
\brief Macro para seleccionar el valor máximo entre dos escalares.
\param[in] a Un número.
\param[in] b Otro número.
\return El mayor de los dos argumentos de entrada.
\date 25 de septiembre de 2009: Creación de la macro.
*/
#define RLS_MAX(a,b) ((a)>(b) ? (a) : (b))
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_MIN
\brief Macro para seleccionar el valor mínimo entre dos escalares.
\param[in] a Un número.
\param[in] b Otro número.
\return El menor de los dos argumentos de entrada.
\date 25 de septiembre de 2009: Creación de la macro.
*/
#define RLS_MIN(a,b) ((a)<(b) ? (a) : (b))
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_ES_CERO
\brief Macro para comprobar si un número puede considerarse cero con una cierta
       tolerancia.
\param[in] num Número a comprobar.
\param[in] tol Tolerancia. Ha de ser un número \b POSITIVO.
\return Dos posibilidades:
        - 0: \em num es distinto de 0, tal que \f$num<=-tol\f$ o \f$num>=tol\f$.
        - 1: \em num es 0, tal que \f$ -tol < num < tol\f$.
\note Para que esta macro funcione correctamente, \em tol ha de ser un número
      \b POSITIVO.
\date 13 de marzo de 2010: Creación de la macro.
\todo Esta macro todavía no está probada.
*/
#define RLS_ES_CERO(num,tol) (((num)>(-(tol)))&&((num)<(tol)) ? 1 : 0)
/******************************************************************************/
/******************************************************************************/
/**
\brief Devuelve el número que representa el valor \em Not-a-Number (\em NaN).
\return Valor NaN.
\note Esta función devuelve el valor almacenado en la constante #RLS_NAN.
\date 21 de diciembre de 2010: Creación de la función.
\date 24 de mayo de 2011: Ahora la función devuelve el valor absoluto de
      #RLS_NAN, calculado con la función <tt>fabs()</tt> de C estándar. Se ha
      hecho así porque, a veces, al imprimir un valor normal de #RLS_NAN, éste
      aparecía con un signo negativo delante.
\date 22 de septiembre de 2011: Lo del <tt>fabs()</tt> no funciona. Parece que
      los problemas en la impresión dependen del compilador y los flags de
      optimización utilizados. No obstante, se mantiene el uso de la función
      <tt>fabs()</tt> en el código.
\todo Esta función todavía no está probada.
*/
double RlsNan(void);
/******************************************************************************/
/******************************************************************************/
/**
\brief Comprueba si un número es \em Not-a-Number (\em NaN).
\param[in] valor Un número.
\return Dos posibilidades:
        - 0: El número pasado no es NaN.
        - Distinto de 0: El número pasado sí es NaN.
\date 21 de diciembre de 2010: Creación de la función.
\date 22 de mayo de 2012: Uso interno de la función \em isnan, de \b C99.
\todo Esta función todavía no está probada.
*/
int EsRlsNan(const double valor);
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
