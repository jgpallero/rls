/* -*- coding: utf-8 -*- */
/**
\file dpeucker.h
\brief Declaración de funciones para el aligerado de polilíneas mediante un
       algoritmo basado en el de Douglas-Peucker sobre el plano y la esfera.
\author José Luis García Pallero, jgpallero@gmail.com
\note Este fichero contiene funciones paralelizadas con OpenMP.
\date 21 de marzo de 2014
\section Licencia Licencia
Copyright 2014, José Luis García Pallero, jgpallero@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _DPEUCKER_H_
#define _DPEUCKER_H_
/******************************************************************************/
/******************************************************************************/
#include<stdlib.h>
#include"dpeuckera.h"
#include"dpeuckerp.h"
#include"dpeuckers.h"
#include"errores.h"
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\brief Elimina vértices de una polilínea mediante una familia de algoritmos
       basados en el de Douglas-Peucker.
\param[in] x Vector que contiene las coordenadas X o las longitudes, en
           radianes, de los vértices de la polilínea de trabajo.
\param[in] y Vector que contiene las coordenadas Y o las latitudes, en radianes,
           de los vértices de la polilínea de trabajo.
\param[in] nPtos Número de elementos de los vectores \em x e \em y.
\param[in] incX Posiciones de separación entre los elementos del vector \em x.
           Este argumento siempre ha de ser un número positivo.
\param[in] incY Posiciones de separación entre los elementos del vector \em y.
           Este argumento siempre ha de ser un número positivo.
\param[in] tol Tolerancia para eliminar vértices. Dos posibilidades:
           - Si se trabaja en coordenadas planas, este argumento ha de estar en
             las mismas unidades que las coordenadas de los vértices.
           - Si se trabaja sobre la esfera, este argumento ha de ser una
             longitud de arco de círculo máximo sobre la esfera de radio unidad.
\param[in] paralelizaTol Identificador para evaluar o no en paralelo si los
           puntos candidatos están en tolerancia. Dos posibilidades:
           - 0: Se evalúa en serie (aunque la compilación se haya hecho en
             paralelo) si los puntos están en tolerancia.
           - Distinto de 0: Se evalúa en paralelo (sólo si se ha compilado en
             paralelo) si los puntos están en tolerancia.
\param[in] robusto Identificador para realizar o no un aligerado robusto. Ha de
           ser un elemento del tipo enumerado #RLS_DPEUCKER_ROBUSTO. Varias
           posibilidades:
           - #RlsDPeuckerOriginal: Utiliza el algoritmo de Douglas-Peucker
             original, que no es robusto.
           - #RlsDPeuckerRobNo: Utiliza la variación no recursiva del algoritmo
             de Douglas-Peucker, que no es robusta.
           - #RlsDPeuckerRobSi: Se aplica el algoritmo robusto completo, que
             garantiza la no ocurrencia de auto intersecciones en la polilínea
             resultante. Internamente, primero se aplica el tratamiento robusto
             de la opción #RlsDPeuckerRobOrig y luego el de la opción
             #RlsDPeuckerRobAuto.
           - #RlsDPeuckerRobOrig: Se aplica un algoritmo semi robusto que
             consiste en garantizar que los segmentos/arcos de la polilínea
             aligerada que se van creando no intersectarán con ninguno de los
             segmentos/arcos que forman los vértices que quedan por procesar de
             la polilínea original. En casos muy especiales, este algoritmo
             puede seguir dando lugar a auto intersecciones.
           - #RlsDPeuckerRobAuto: Se aplica un algoritmo semi robusto que
             consiste en garantizar que los segmentos/arcos de la polilínea
             aligerada que se van creando no intersectarán con ninguno de los
             segmentos/arcos de la polilínea aligerada creados con anterioridad.
             En casos muy especiales, este algoritmo puede seguir dando lugar a
             auto intersecciones.
\param[in] nSegRobOrig Número de segmentos/arcos de la polilínea original a
           utilizar en el caso de tratamiento robusto con las opciones
           #RlsDPeuckerRobSi o #RlsDPeuckerRobOrig. Si se pasa el valor 0, se
           utilizan todos los segmentos/arcos hasta el final de la polilínea
           original.
\param[in] nSegRobAuto Número de segmentos de la polilínea aligerada a utilizar
           en el caso de tratamiento robusto con las opciones #RlsDPeuckerRobSi
           o #RlsDPeuckerRobAuto. Si se pasa el valor 0, se utilizan todos los
           segmentos hasta el inicio de la polilínea aligerada.
\param[in] esf Identificador de trabajo sobre la superficie de la esfera. Dos
           posibilidades:
           - 0: No se trabaja sobre la superficie de la esfera, sino en el
             plano.
           - Distinto de 0: Se trabaja sobre la superficie de la esfera de radio
             unidad.
\param[out] nPtosSal Número de puntos de la polilínea aligerada.
\return Vector de \em nPtosSal elementos que contiene los índices en los
        vectores \em x e \em y de los vértices que formarán la polilínea
        aligerada. Si ocurre algún error de asignación de memoria se devuelve el
        valor \p NULL.
\note Esta función no comprueba si el número de elementos de los vectores \em x
      e \em y es congruente con el valor pasado en \em nPtos.
\note Esta función asume que \em nPtos es mayor que 0. En caso contrario,
      devuelve \p NULL, por lo que un valor de retorno igual a \p NULL sólo es
      indicativo de error cuando \em nPtos es mayor que 0.
\note Esta función comprueba los casos especiales con
      \ref CasosEspecialesAligeraPolilinea.
\note El argumento \em paralelizaTol \b SÓLO afecta a la paralelización de la
      comprobación de puntos en tolerancia. Los chequeos de intersección de
      segmentos/arcos siempre se hacen en paralelo (si el código ha sido
      compilado al efecto).
\date 25 de mayo de 2012: Creación de la función.
\date 08 de agosto de 2013: Comprobación de casos especiales.
\date 17 de agosto de 2013: Unificación de las funciones de aligerado en el
      plano y en la esfera.
\date 20 de agosto de 2013: Sustitución de las antiguas variables de entrada
      \em nPtosRobusto y \em nSegRobusto por \em nSegRobOrig y \em nSegRobAuto.
\date 23 de agosto de 2013: Adición del argumento de entrada \em paralelizaTol.
\date 21 de septiembre de 2013: Adición de la capacidad de trabajar sobre la
      esfera con el algoritmo de Douglas-Peucker original.
\todo Esta función todavía no está probada.
*/
int* AligeraPolilinea(const double* x,
                      const double* y,
                      const int nPtos,
                      const int incX,
                      const int incY,
                      const double tol,
                      const int paralelizaTol,
                      const enum RLS_DPEUCKER_ROBUSTO robusto,
                      const int nSegRobOrig,
                      const int nSegRobAuto,
                      const int esf,
                      int* nPtosSal);
/******************************************************************************/
/******************************************************************************/
/**
\brief Elimina vértices de una polilínea mediante el algoritmo original de
       Douglas-Peucker.
\param[in] x Vector que contiene las coordenadas X o las longitudes, en
           radianes, de los vértices de la polilínea de trabajo.
\param[in] y Vector que contiene las coordenadas Y o las latitudes, en radianes,
           de los vértices de la polilínea de trabajo.
\param[in] nPtos Número de elementos de los vectores \em x e \em y.
\param[in] incX Posiciones de separación entre los elementos del vector \em x.
           Este argumento siempre ha de ser un número positivo.
\param[in] incY Posiciones de separación entre los elementos del vector \em y.
           Este argumento siempre ha de ser un número positivo.
\param[in] tol Tolerancia para eliminar vértices. Dos posibilidades:
           - Si se trabaja en coordenadas planas, este argumento ha de estar en
             las mismas unidades que las coordenadas de los vértices.
           - Si se trabaja sobre la esfera, este argumento ha de ser una
             longitud de arco de círculo máximo sobre la esfera de radio unidad.
\param[in] esf Identificador de trabajo sobre la superficie de la esfera. Dos
           posibilidades:
           - 0: No se trabaja sobre la superficie de la esfera, sino en el
             plano.
           - Distinto de 0: Se trabaja sobre la superficie de la esfera de radio
             unidad.
\param[out] nPtosSal Número de puntos de la polilínea aligerada.
\return Vector de \em nPtosSal elementos que contiene los índices en los
        vectores \em x e \em y de los vértices que formarán la polilínea
        aligerada. Si ocurre algún error de asignación de memoria se devuelve el
        valor \p NULL.
\note Esta función no comprueba si el número de elementos de los vectores \em x
      e \em y es congruente con el valor pasado en \em nPtos.
\note Esta función asume que \em nPtos es mayor que 0. En caso contrario,
      devuelve \p NULL, por lo que un valor de retorno igual a \p NULL sólo es
      indicativo de error cuando \em nPtos es mayor que 0.
\note Esta función puede devolver resultados erróneos si algún segmento base es
      mayor o igual que \f$\pi\f$.
\date 21 de septiembre de 2013: Creación de la función.
\todo Esta función todavía no está probada.
*/
int* DouglasPeuckerOriginal(const double* x,
                            const double* y,
                            const int nPtos,
                            const int incX,
                            const int incY,
                            const double tol,
                            const int esf,
                            int* nPtosSal);
/******************************************************************************/
/******************************************************************************/
/**
\brief Elimina vértices de una polilínea mediante un algoritmo no recursivo,
       inspirado en el de Douglas-Peucker.
\brief Este algoritmo, comenzando por el primer punto de la polilínea, va
       uniendo puntos en segmentos/arcos de tal forma que se eliminan todos
       aquellos puntos que queden a una distancia menor o igual a \em tol del
       segmento/arco de trabajo. Así aplicado, pueden ocurrir casos singulares
       en los que la polilínea aligerada tenga casos de auto intersección entre
       sus lados resultantes. Para evitar esto, se puede aplicar la versión
       robusta del algoritmo.
\param[in] x Vector que contiene las coordenadas X o las longitudes, en
           radianes, de los vértices de la polilínea de trabajo.
\param[in] y Vector que contiene las coordenadas Y o las latitudes, en radianes,
           de los vértices de la polilínea de trabajo.
\param[in] nPtos Número de elementos de los vectores \em x e \em y.
\param[in] incX Posiciones de separación entre los elementos del vector \em x.
           Este argumento siempre ha de ser un número positivo.
\param[in] incY Posiciones de separación entre los elementos del vector \em y.
           Este argumento siempre ha de ser un número positivo.
\param[in] tol Tolerancia para eliminar vértices. Dos posibilidades:
           - Si se trabaja en coordenadas planas, este argumento ha de estar en
             las mismas unidades que las coordenadas de los vértices.
           - Si se trabaja sobre la esfera, este argumento ha de ser una
             longitud de arco de círculo máximo sobre la esfera de radio unidad.
\param[in] paralelizaTol Identificador para evaluar o no en paralelo si los
           puntos candidatos están en tolerancia. Dos posibilidades:
           - 0: Se evalúa en serie (aunque la compilación se haya hecho en
             paralelo) si los puntos están en tolerancia.
           - Distinto de 0: Se evalúa en paralelo (sólo si se ha compilado en
             paralelo) si los puntos están en tolerancia.
\param[in] robusto Identificador para realizar o no un aligerado robusto. Ha de
           ser un elemento del tipo enumerado #RLS_DPEUCKER_ROBUSTO. Varias
           posibilidades:
           - #RlsDPeuckerOriginal: En este caso esta opción es equivalente a
             pasar #RlsDPeuckerRobNo.
           - #RlsDPeuckerRobNo: Utiliza la variación no recursiva del algoritmo
             de Douglas-Peucker, que no es robusta.
           - #RlsDPeuckerRobSi: Se aplica el algoritmo robusto completo, que
             garantiza la no ocurrencia de auto intersecciones en la polilínea
             resultante. Internamente, primero se aplica el tratamiento robusto
             de la opción #RlsDPeuckerRobOrig y luego el de la opción
             #RlsDPeuckerRobAuto.
           - #RlsDPeuckerRobOrig: Se aplica un algoritmo semi robusto que
             consiste en garantizar que los segmentos/arcos de la polilínea
             aligerada que se van creando no intersectarán con ninguno de los
             segmentos/arcos que forman los vértices que quedan por procesar de
             la polilínea original. En casos muy especiales, este algoritmo
             puede seguir dando lugar a auto intersecciones.
           - #RlsDPeuckerRobAuto: Se aplica un algoritmo semi robusto que
             consiste en garantizar que los segmentos/arcos de la polilínea
             aligerada que se van creando no intersectarán con ninguno de los
             segmentos/arcos de la polilínea aligerada creados con anterioridad.
             En casos muy especiales, este algoritmo puede seguir dando lugar a
             auto intersecciones.
\param[in] nSegRobOrig Número de segmentos/arcos de la polilínea original a
           utilizar en el caso de tratamiento robusto con las opciones
           #RlsDPeuckerRobSi o #RlsDPeuckerRobOrig. Si se pasa el valor 0, se
           utilizan todos los segmentos/arcos hasta el final de la polilínea
           original.
\param[in] nSegRobAuto Número de segmentos de la polilínea aligerada a utilizar
           en el caso de tratamiento robusto con las opciones #RlsDPeuckerRobSi
           o #RlsDPeuckerRobAuto. Si se pasa el valor 0, se utilizan todos los
           segmentos hasta el inicio de la polilínea aligerada.
\param[in] esf Identificador de trabajo sobre la superficie de la esfera. Dos
           posibilidades:
           - 0: No se trabaja sobre la superficie de la esfera, sino en el
             plano.
           - Distinto de 0: Se trabaja sobre la superficie de la esfera de radio
             unidad.
\param[out] nPtosSal Número de puntos de la polilínea aligerada.
\return Vector de \em nPtosSal elementos que contiene los índices en los
        vectores \em x e \em y de los vértices que formarán la polilínea
        aligerada. Si ocurre algún error de asignación de memoria se devuelve el
        valor \p NULL.
\note Esta función no comprueba si el número de elementos de los vectores \em x
      e \em y es congruente con el valor pasado en \em nPtos.
\note Esta función asume que \em nPtos es mayor que 0. En caso contrario,
      devuelve \p NULL, por lo que un valor de retorno igual a \p NULL sólo es
      indicativo de error cuando \em nPtos es mayor que 0.
\note Esta función comprueba los casos especiales con
      \ref CasosEspecialesAligeraPolilinea.
\note El argumento \em paralelizaTol \b SÓLO afecta a la paralelización de la
      comprobación de puntos en tolerancia. Los chequeos de intersección de
      segmentos/arcos siempre se hacen en paralelo (si el código ha sido
      compilado al efecto).
\date 07 de julio de 2011: Creación de la función.
\date 10 de julio de 2011: Cambio del tipo del argumento \em robusto al tipo
      enumerado #RLS_DPEUCKER_ROBUSTO.
\date 14 de mayo de 2012: Corregido bug que hacía que no se escogiese bien el
      vértice a añadir a la polilínea aligerada.
\date 25 de mayo de 2012: Cambio de nombre de la función.
\date 17 de agosto de 2013: Comprobación de casos especiales y unificación de
      las funciones de aligerado en el plano y en la esfera.
\date 20 de agosto de 2013: Sustitución de las antiguas variables de entrada
      \em nPtosRobusto y \em nSegRobusto por \em nSegRobOrig y \em nSegRobAuto.
\date 23 de agosto de 2013: Adición del argumento de entrada \em paralelizaTol.
\todo Esta función todavía no está probada.
*/
int* DouglasPeuckerRobusto(const double* x,
                           const double* y,
                           const int nPtos,
                           const int incX,
                           const int incY,
                           const double tol,
                           const int paralelizaTol,
                           const enum RLS_DPEUCKER_ROBUSTO robusto,
                           const int nSegRobOrig,
                           const int nSegRobAuto,
                           const int esf,
                           int* nPtosSal);
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
