/* -*- coding: utf-8 -*- */
/**
\file errores.h
\brief Declaración de funciones y constantes para el tratamiento de errores.

En el momento de la compilación ha de seleccionarse el comportamiento de la
función \ref RlsError. Para realizar la selección es necesario definir las
variables para el preprocesador \em ESCRIBE_MENSAJE_ERROR si se quiere que la
función imprima un mensaje de error y/o \em FIN_PROGRAMA_ERROR si se quiere que
la función termine la ejecución del programa en curso. Si no se define ninguna
variable, la función no ejecuta ninguna acción. En \p gcc, las variables para el
preprocesador se pasan como \em -DXXX, donde \em XXX es la variable a
introducir.
\author José Luis García Pallero, jgpallero@gmail.com
\date 06 de marzo de 2009
\section Licencia Licencia
Copyright 2013-2014, José Luis García Pallero, jgpallero@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
/******************************************************************************/
/******************************************************************************/
#ifndef _ERRORES_H_
#define _ERRORES_H_
/******************************************************************************/
/******************************************************************************/
//GENERAL
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_PLINEA
\brief Palabra \em Línea para ser utilizada en el mensaje que imprime la macro
       #RLS_ERROR. Esta constante se define porque el preprocesador del
       compilador \p pgcc no soporta letras con tilde escritas directamente en
       las órdenes a ejecutar por las macros.
\date 10 de enero de 2011: Creación de la constante.
*/
#define RLS_PLINEA "Línea"
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_TIPO_ERR_NADA
\brief Indicador de que la función \ref RlsError no hace nada.
\date 09 de enero de 2011: Creación de la constante.
*/
#define RLS_TIPO_ERR_NADA 0
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_TIPO_ERR_MENS_Y_EXIT
\brief Indicador de que la función \ref RlsError imprime un mensaje descriptivo
       en la salida de error \em stderr y termina la ejecución del programa en
       curso.
\date 09 de enero de 2011: Creación de la constante.
*/
#define RLS_TIPO_ERR_MENS_Y_EXIT 1
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_TIPO_ERR_MENS
\brief Indicador de que la función \ref RlsError imprime un mensaje descriptivo
       en la salida de error \em stderr y no termina la ejecución del programa
       en curso.
\date 09 de enero de 2011: Creación de la constante.
*/
#define RLS_TIPO_ERR_MENS 2
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_TIPO_ERR_EXIT
\brief Indicador de que la función \ref RlsError termina la ejecución del
       programa en curso.
\date 09 de enero de 2011: Creación de la constante.
*/
#define RLS_TIPO_ERR_EXIT 3
/******************************************************************************/
/******************************************************************************/
/**
\brief Indica el tipo de acción que realiza la función \ref RlsError.
\return Cuatro posibles valores:
        - #RLS_TIPO_ERR_NADA: La función \ref RlsError no hace nada.
        - #RLS_TIPO_ERR_MENS_Y_EXIT: La función \ref RlsError imprime un
          mensaje descriptivo en la salida de error \em stderr y termina la
          ejecución del programa en curso.
        - #RLS_TIPO_ERR_MENS: La función \ref RlsError imprime un mensaje
          descriptivo en la salida de error \em stderr y no detiene la ejecución
          del programa en curso.
        - #RLS_TIPO_ERR_EXIT: La función \ref RlsError detiene la ejecución
          del programa en curso.
\date 09 de enero de 2011: Creación de la función.
*/
int RlsTipoError(void);
/******************************************************************************/
/******************************************************************************/
/**
\brief Imprime un mensaje en la salida de error \em stderr y/o sale del programa
       en ejecución.
\param[in] mensaje Cadena de texto a imprimir.
\param[in] funcion Nombre de la función desde donde se ha invocado a esta
           función.
\note Si este fichero se compila con las variables para el preprocesador
      \em ESCRIBE_MENSAJE_ERROR y \em FIN_PROGRAMA_ERROR, esta función imprime
      el mensaje de error y termina la ejecución del programa en curso mediante
      la llamada a la función \em exit(EXIT_FAILURE), de la biblioteca estándar
      de C.
\note Si este fichero se compila con la variable para el preprocesador
      \em ESCRIBE_MENSAJE_ERROR, esta función imprime el mensaje de error.
\note Si este fichero se compila con la variable para el preprocesador
      \em FIN_PROGRAMA_ERROR, esta función termina la ejecución del programa en
      curso mediante la llamada a la función \em exit(EXIT_FAILURE), de la
      biblioteca estándar de C.
\note Si este fichero se compila sin variables para el preprocesador, esta
      función no hace nada.
\date 10 de enero de 2011: Creación de la función.
*/
void RlsError(const char mensaje[],
              const char funcion[]);
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_ERROR
\brief Macro para imprimir un mensaje en la salida de error \em stderr y/o sale
       del programa en ejecución.
\param[in] mensaje Cadena de texto a imprimir.
\note Esta macro llama internamente a la función \ref RlsError.
\note Esta macro pasa como argumento \em funcion a \ref RlsError la variable
      del preprocesador \em __func__, de C99.
\date 10 de enero de 2011: Creación de la macro.
*/
#define RLS_ERROR(mensaje) \
{ \
    if(RlsTipoError()!=RLS_TIPO_ERR_NADA) \
    { \
        fprintf(stderr,"\n\n"); \
        fprintf(stderr,"********************\n********************\n"); \
        fprintf(stderr,RLS_PLINEA" %d del fichero '%s'\n",__LINE__,__FILE__); \
        RlsError(mensaje,(const char*)__func__); \
        fprintf(stderr,"********************\n********************\n\n"); \
    } \
    else \
    { \
        RlsError(mensaje,(const char*)__func__); \
    } \
}
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
