/* -*- coding: utf-8 -*- */
/**
\file dpeuckera.h
\brief Declaración de elementos y funciones auxiliares para el uso de la familia
       de algoritmos de Douglas-Peucker.
\author José Luis García Pallero, jgpallero@gmail.com
\date 04 de abril de 2014
\section Licencia Licencia
Copyright 2014, José Luis García Pallero, jgpallero@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _DPEUCKERA_H_
#define _DPEUCKERA_H_
/******************************************************************************/
/******************************************************************************/
#include<stdlib.h>
#include<math.h>
#include"errores.h"
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_DPEUCKER_BUFFER_PTOS
\brief Número de puntos para ir asignando memoria en bloques para el vector de
       salida de las funciones de aligerado de polilíneas.
\date 17 de agosto de 2013: Creación de la constante.
*/
#define RLS_DPEUCKER_BUFFER_PTOS 1000
/******************************************************************************/
/******************************************************************************/
/**
\def RLS_DPEUCKER_NO_INTERSEC
\brief Identificador de que dos segmentos o arcos no se cortan.
\date 18 de agosto de 2013: Creación de la constante.
*/
#define RLS_DPEUCKER_NO_INTERSEC 0
/******************************************************************************/
/******************************************************************************/
/** \enum RLS_DPEUCKER_ROBUSTO
\brief Aplicación o no del algoritmo robusto de aligerado de polilíneas.
\date 10 de julio de 2011: Creación del tipo.
\date 24 de mayo de 2011: Adición del la opción \em RlsDPeuckerOriginal.
*/
enum RLS_DPEUCKER_ROBUSTO
{
    /** \brief Se usa el algoritmo original de Douglas-Peucker (no robusto). */
    RlsDPeuckerOriginal=111,
    /** \brief \b *NO* se realiza aligerado robusto (con mi algoritmo). */
    RlsDPeuckerRobNo=112,
    /** \brief \b *SÍ* se realiza aligerado robusto. */
    RlsDPeuckerRobSi=113,
    /** \brief Aligerado semi robusto con
        \ref DouglasPeuckerRobIntersecOrigPlano y
        \ref DouglasPeuckerRobIntersecOrigEsfera. */
    RlsDPeuckerRobOrig=114,
    /** \brief Aligerado semi robusto con
        \ref DouglasPeuckerRobAutoIntersecPlano y
        \ref DouglasPeuckerRobAutoIntersecEsfera. */
    RlsDPeuckerRobAuto=115
};
/******************************************************************************/
/******************************************************************************/
/**
\brief Comprueba algunos casos especiales antes del aligerado de vértices de una
       polilínea.
\param[in] x Vector que contiene las coordenadas X o las longitudes de los
           vértices de la polilínea de trabajo.
\param[in] y Vector que contiene las coordenadas Y o las latitudes de los
           vértices de la polilínea de trabajo.
\param[in] nPtos Número de elementos de los vectores \em x e \em y.
\param[in] incX Posiciones de separación entre los elementos del vector \em x.
           Este argumento siempre ha de ser un número positivo.
\param[in] incY Posiciones de separación entre los elementos del vector \em y.
           Este argumento siempre ha de ser un número positivo.
\param[in] tol Tolerancia para eliminar vértices. Este argumento sólo se tiene
           internamente en cuenta si vale 0.0.
\param[out] nPtosSal Número de puntos de la polilínea aligerada, si se ha
            encontrado algún caso especial.
\param[out] hayCasoEspecial Identificador de caso especial. Dos posibilidades:
            - 0: No se ha encontrado ningún caso especial.
            - Distinto de 0: Sí se ha encontrado algún caso especial.
\return Vector de \em nPtosSal elementos que contiene los índices en los
        vectores \em x e \em y de los vértices que formarán la polilínea
        aligerada, en el caso de que se haya encontrado algún caso especial. Si
        ocurre algún error de asignación de memoria se devuelve el valor
        \p NULL. Este argumento sólo tiene sentido si se ha devuelto un valor de
        \em hayCasoEspecial distinto de 0.
\note Esta función no comprueba si el número de elementos de los vectores \em x
      e \em y es congruente con el valor pasado en \em nPtos.
\note Esta función asume que \em nPtos es mayor que 0. En caso contrario,
      devuelve \p NULL, por lo que un valor de retorno igual a \p NULL sólo es
      indicativo de error cuando \em nPtos es mayor que 0.
\note Esta función comprueba los siguientes casos especiales:
      - Si sólo hay un punto de entrada: En este caso, \em nPtosSal vale 1 y el
        vector de salida contiene la posición de ese único punto.
      - Si hay dos puntos de entrada, y estos son el mismo: En este caso,
        \em nPtosSal vale 1 y el vector de salida contiene la posición
        únicamente del primero de ellos.
      - Si hay dos puntos de entrada, y estos son distintos: En este caso,
        \em nPtosSal vale 2 y el vector de salida contiene la posición de los
        dos puntos, aunque la distancia entre ellos sea menor que \em tol.
      - Si hay tres puntos de entrada, y el primero es el mismo que el tercero:
        En este caso, \em nPtosSal vale 2 y el vector de salida contiene la
        posición de los dos primeros.
        - En el caso en que el segundo punto también sea igual al primero,
          \em nPtosSal vale 1 y el vector de salida contiene la posición
          únicamente del primero.
      - Si \em tol vale 0.0: En este caso, se eliminan los posibles puntos
        repetidos en los vectores de coordenadas de entrada.
\date 08 de agosto de 2013: Creación de la función.
\todo Esta función todavía no está probada.
*/
int* CasosEspecialesAligeraPolilinea(const double* x,
                                     const double* y,
                                     const int nPtos,
                                     const int incX,
                                     const int incY,
                                     const double tol,
                                     int* nPtosSal,
                                     int* hayCasoEspecial);
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
