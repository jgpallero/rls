/* -*- coding: utf-8 -*- */
/**
\file mygetopt.h
\brief Declaración de funciones de \p mygetopt. \p mygetopt ha sido tomada de
       http://myman.sourceforge.net/
\author Benjamin C. Wiley Sittler, http://myman.sourceforge.net/
\date 02 de abril de 2011
\version 1.0
\section Licencia Licencia
mygetopt.h - interface to my re-implementation of getopt.
Copyright 1997, 2000, 2001, 2002, 2006, 2008, 2009, Benjamin C. Wiley Sittler

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/
/******************************************************************************/
/******************************************************************************/
/*  mygetopt.h - interface to my re-implementation of getopt.
 *  Copyright 1997, 2000, 2001, 2002, 2006, 2008, 2009, Benjamin C. Wiley Sittler
 *
 *  Permission is hereby granted, free of charge, to any person
 *  obtaining a copy of this software and associated documentation
 *  files (the "Software"), to deal in the Software without
 *  restriction, including without limitation the rights to use, copy,
 *  modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 *  DEALINGS IN THE SOFTWARE.
 */

#ifndef MYGETOPT_H_INCLUDED
#define MYGETOPT_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

/* reset argument parser to start-up values */
extern int mygetopt_reset(void);

/* UNIX-style short-argument parser */
extern int mygetopt(int argc, char * argv[], const char *opts);

extern int my_optind, my_opterr, my_optopt, my_optreset;
extern char *my_optarg;

struct option {
  const char *name;
  int has_arg;
  int *flag;
  int val;
};

/* human-readable values for has_arg */
#ifdef no_argument
#undef no_argument
#endif
#define no_argument 0
#ifdef required_argument
#undef required_argument
#endif
#define required_argument 1
#ifdef optional_argument
#undef optional_argument
#endif
#define optional_argument 2

/* GNU-style long-argument parsers */
extern int mygetopt_long(int argc, char * argv[], const char *shortopts,
                       const struct option *longopts, int *longind);

extern int mygetopt_long_only(int argc, char * argv[], const char *shortopts,
                            const struct option *longopts, int *longind);

extern int _mygetopt_internal(int argc, char * argv[], const char *shortopts,
                            const struct option *longopts, int *longind,
                            int long_only);

#ifdef __cplusplus
}
#endif

#endif /* MYGETOPT_H_INCLUDED */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
