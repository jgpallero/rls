# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#Copyright 2013-2014, José Luis García Pallero, jgpallero@gmail.com
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.
#-------------------------------------------------------------------------------
#VARIABLES INTERNAS
#extensión de los binarios
EXT=
#empaquetador y sus opciones
AR=ar
ARFLAGS=rcs
#directorio de archivos de cabecera
DIRINC=./include
#directorio de código de la biblioteca
DIRSRC=./src
#directorio de código del programa
DIRPROG=./prog
#directorio para la biblioteca
DIRLIB=./lib
#directorio para binarios
DIRBIN=./bin
#nombre de la biblioteca a crear
NOMLIB=rls
#flags de control de errores
CFLAGSERR=-DESCRIBE_MENSAJE_ERROR
#-------------------------------------------------------------------------------
#VALORES POR DEFECTO DE LAS VARIABLES DEL MAKEFILE
#compilador
CC=gcc
#estándar del lenguaje
LSTD=c99
#paralelización con OpenMP
PAROMP=n
#cálculo de tiempo con OpenMP
TIEMPO=
#procesador (por defecto, general)
PROC=general
#sistema operativo (por defecto, no windows)
TARGET=gnu
#enlazado del binario final
LINK=
#nombre del programa a crear
NOMPRG=$(DIRBIN)/rls
#-------------------------------------------------------------------------------
#comprobamos si hay que calcular tiempo con las funciones de OpenMP
CFTOMP=
ifeq ($(TIEMPO),omp)
	CFTOMP=-DCTOMP
endif
ifeq ($(PAROMP),s)
	override TIEMPO=omp
	CFTOMP=-DCTOMP
endif
export TIEMPO
#-------------------------------------------------------------------------------
#SELECCIÓN DE PARÁMETROS DEL COMPILADOR
include Makefile.cmp
#-------------------------------------------------------------------------------
#ÓRDENES DE COMPILACIÓN
#etiqueta para construir todo
.PHONY: all
all: avisoomp directorios lib prog
#posible warning por no soportar OpenMP
.PHONY: avisoomp
avisoomp:
ifneq ($(AVISONOOMP),no)
	$(error $(AVISONOOMP))
endif
#etiqueta para construir los directorios necesarios para la compilación
.PHONY: directorios
directorios:
	chmod 755 initials
	./initials
#etiqueta para construir la biblioteca
.PHONY: lib
lib: directorios
	$(CC) $(CFLAGS) -I$(DIRINC) -c \
	$(DIRSRC)/mygetopt.c -o $(DIRSRC)/mygetopt.o
	$(CC) $(CFLAGS) $(CFLAGSERR) -I$(DIRINC) -c \
	$(DIRSRC)/errores.c -o $(DIRSRC)/errores.o
	$(CC) $(CFLAGS) -I$(DIRINC) -c \
	$(DIRSRC)/fgeneral.c -o $(DIRSRC)/fgeneral.o
	$(CC) $(CFLAGS) -I$(DIRINC) -c \
	$(DIRSRC)/arco.c -o $(DIRSRC)/arco.o
	$(CC) $(CFLAGS) -I$(DIRINC) -c \
	$(DIRSRC)/segmento.c -o $(DIRSRC)/segmento.o
	$(CC) $(CFLAGS) $(CFLAGSOMP) -I$(DIRINC) -c \
	$(DIRSRC)/dpeuckerp.c -o $(DIRSRC)/dpeuckerp.o
	$(CC) $(CFLAGS) $(CFLAGSOMP) -I$(DIRINC) -c \
	$(DIRSRC)/dpeuckers.c -o $(DIRSRC)/dpeuckers.o
	$(CC) $(CFLAGS) -I$(DIRINC) -c \
	$(DIRSRC)/dpeuckera.c -o $(DIRSRC)/dpeuckera.o
	$(CC) $(CFLAGS) -I$(DIRINC) -c \
	$(DIRSRC)/dpeucker.c -o $(DIRSRC)/dpeucker.o
	$(AR) $(ARFLAGS) $(DIRLIB)/lib$(NOMLIB).a $(DIRSRC)/*.o
#etiqueta para construir el programa
.PHONY: prog
prog: directorios
	$(CC) $(CFLAGSP) -I$(DIRINC) -L$(DIRLIB) $(CFTOMP) \
	$(DIRPROG)/rls.c -o $(NOMPRG)$(EXT) -l$(NOMLIB) -lm $(CFLINKOMP)
#etiqueta para limpiar basura
.PHONY: clean
clean:
	rm -rf *~ *.o *.oo $(DIRINC)/*~ $(DIRSRC)/*~ $(DIRSRC)/*.o $(DIRPROG)/*~
#etiqueta para dejarlo todo como al principio
.PHONY: cleanall
cleanall: clean
	rm -rf $(DIRLIB)/ $(DIRBIN)/
#-------------------------------------------------------------------------------
#ayuda
.PHONY: help
help:
	@echo " Compilation Makefile for RLS"
	@echo
	@echo " Targets:"
	@echo "    - all: Creates the library $(DIRLIB)/lib$(NOMLIB).a and the"
	@echo "           program $(NOMPRG)"
	@echo "    - lib: Creates the library $(DIRLIB)/lib$(NOMLIB).a"
	@echo "    - prog: Creates the program $(NOMPRG)"
	@echo "    - clean: Deletes the compilation results, except the files"
	@echo "             $(DIRLIB)/lib$(NOMLIB).a and $(NOMPRG)"
	@echo "    - cleanall: Deletes all the compilation results"
	@echo "    - help: Shows this help"
	@echo
	@echo " Variables:"
	@echo "    CC: Compiler to use. There are defined:"
	@echo "        - Free: gcc, clang, llvm-gcc, owcc, opencc, pathcc, tcc,"
	@echo "                pcc, plaincc, nwcc, vc and tendracc"
	@echo "        - Non-free: icc, suncc, pgcc, xlc y lcc"
	@echo "        By default: $(CC)"
	@echo "    PAROMP: Identifier for compiling the parallel OpenMP version."
	@echo "            Two possibilities:"
	@echo "            - s: Compilation with OpenMP"
	@echo "            - Other than s: Compilation without OpenMP"
	@echo "            By default: $(PAROMP)"
	@echo "    TIEMPO: Identifier for the time computation function. Two"
	@echo "            possibilities:"
	@echo "            - omp: The omp_get_wtime() function from OpenMP is used"
	@echo "            - Other than omp: The clock() function from standard C"
	@echo "                              is used (resolution of second)"
	@echo "            By default: $(TIEMPO)"
	@echo "            Note: If PAROMP=s is selected, TIEMPO is forced to"
	@echo "                  TIEMPO=omp"
	@echo "    PROC: Type of the used processor. Various possibilities:"
	@echo "          - general: Generic compilation"
	@echo "          - local: Optimized compilation for the local processor"
	@echo "                   (only if the compiler permits this option)"
	@echo "          - XXX: Optimized compilation for the architecture stated"
	@echo "                 in XXX (only if the compiler permits this option)"
	@echo "          By default: $(PROC)"
	@echo "    TARGET: Machine identifier. Various possibilities:"
	@echo "            - 32bits: Compilation for 32 bits machine"
	@echo "            - 64bits: Compilation for 64 bits machine"
	@echo "            - win32: Cross compiler for Windows (32 bits)"
	@echo "            - win64: Cross compiler for Windows (64 bits)"
	@echo "            Note 1: The options 32bits and 64bits are only taken in"
	@echo "                    account for i386, i486, i586, i686 and x86_64"
	@echo "                    machines"
	@echo "            Note 2: The options of cross compiling for MS Windows"
	@echo "                    are taken in account only if the MinGW"
	@echo "                    environment is present. The compiler must have"
	@echo "                    the names:"
	@echo "                    - i686-w64-mingw32-gcc (or -g++, o -c++)"
	@echo "                    - x86_64-w64-mingw32-gcc (or -g++, o -c++)"
	@echo "            By default: $(TARGET)"
	@echo "    LINK: Identifier for the linker in order to create static or"
	@echo "          shared executable:"
	@echo "          - static: Static executable is created."
	@echo "          By default: $(LINK)"
	@echo "    NOMPRG: Path and name of the executable to create"
	@echo "            By default: $(NOMPRG)"
	@echo
	@echo " (C), 2013, José Luis García Pallero"
#-------------------------------------------------------------------------------
# kate: encoding utf-8; end-of-line unix; syntax makefile; indent-mode cstyle;
# kate: replace-tabs off; space-indent off; tab-indents on; indent-width 4;
# kate: tab-width 4;
# kate: line-numbers on; folding-markers on; remove-trailing-space on;
# kate: backspace-indents on; show-tabs on;
# kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off;
